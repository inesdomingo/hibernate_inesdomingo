package com.inesd.hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Pedido {
    private int idPedido;
    private double precio;
    private byte internet;
    private byte pagado;
    private Date fechaPedido;
    private Regalo regalo;
    private com.inesd.hibernate.Cliente Cliente;
    private com.inesd.hibernate.Empleado Empleado;

    @Id
    @Column(name = "id_pedido")
    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "internet")
    public byte getInternet() {
        return internet;
    }

    public void setInternet(byte internet) {
        this.internet = internet;
    }

    @Basic
    @Column(name = "pagado")
    public byte getPagado() {
        return pagado;
    }

    public void setPagado(byte pagado) {
        this.pagado = pagado;
    }

    @Basic
    @Column(name = "fecha_pedido")
    public Date getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pedido pedido = (Pedido) o;
        return idPedido == pedido.idPedido &&
                Double.compare(pedido.precio, precio) == 0 &&
                internet == pedido.internet &&
                pagado == pedido.pagado &&
                Objects.equals(fechaPedido, pedido.fechaPedido);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPedido, precio, internet, pagado, fechaPedido);
    }

    @ManyToOne
    @JoinColumn(name = "id_regalo", referencedColumnName = "id_regalo", nullable = false)
    public Regalo getRegalo() {
        return regalo;
    }

    public void setRegalo(Regalo regalo) {
        this.regalo = regalo;
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", nullable = false)
    public com.inesd.hibernate.Cliente getCliente() {
        return Cliente;
    }

    public void setCliente(com.inesd.hibernate.Cliente cliente) {
        Cliente = cliente;
    }

    @ManyToOne
    @JoinColumn(name = "id_empleado", referencedColumnName = "id_empleado", nullable = false)
    public com.inesd.hibernate.Empleado getEmpleado() {
        return Empleado;
    }

    public void setEmpleado(com.inesd.hibernate.Empleado empleado) {
        Empleado = empleado;
    }
}
