package com.inesd.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Regalo {
    private int idRegalo;
    private Object tipoRegalo;
    private double precio;
    private byte escritoNombre;
    private byte escritoFecha;
    private Object idioma;
    private List<Pedido> pedidos;
    private List<Tienda> tiendas;

    @Id
    @Column(name = "id_regalo")
    public int getIdRegalo() {
        return idRegalo;
    }

    public void setIdRegalo(int idRegalo) {
        this.idRegalo = idRegalo;
    }

    @Basic
    @Column(name = "tipo_regalo")
    public Object getTipoRegalo() {
        return tipoRegalo;
    }

    public void setTipoRegalo(Object tipoRegalo) {
        this.tipoRegalo = tipoRegalo;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "escrito_nombre")
    public byte getEscritoNombre() {
        return escritoNombre;
    }

    public void setEscritoNombre(byte escritoNombre) {
        this.escritoNombre = escritoNombre;
    }

    @Basic
    @Column(name = "escrito_fecha")
    public byte getEscritoFecha() {
        return escritoFecha;
    }

    public void setEscritoFecha(byte escritoFecha) {
        this.escritoFecha = escritoFecha;
    }

    @Basic
    @Column(name = "idioma")
    public Object getIdioma() {
        return idioma;
    }

    public void setIdioma(Object idioma) {
        this.idioma = idioma;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Regalo regalo = (Regalo) o;
        return idRegalo == regalo.idRegalo &&
                Double.compare(regalo.precio, precio) == 0 &&
                escritoNombre == regalo.escritoNombre &&
                escritoFecha == regalo.escritoFecha &&
                Objects.equals(tipoRegalo, regalo.tipoRegalo) &&
                Objects.equals(idioma, regalo.idioma);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRegalo, tipoRegalo, precio, escritoNombre, escritoFecha, idioma);
    }

    @OneToMany(mappedBy = "regalo")
    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    @ManyToMany
    @JoinTable(name = "regalo_tienda", catalog = "", schema = "tienda_regalos", joinColumns = @JoinColumn(name = "id_tienda", referencedColumnName = "id_regalo", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_regalo", referencedColumnName = "id_tienda", nullable = false))
    public List<Tienda> getTiendas() {
        return tiendas;
    }

    public void setTiendas(List<Tienda> tiendas) {
        this.tiendas = tiendas;
    }
}
