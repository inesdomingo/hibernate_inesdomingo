package com.inesd.hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Empleado {
    private int idEmpleado;
    private String nombre;
    private int edad;
    private Date fechaInicio;
    private List<Pedido> Pedidos;
    private Tienda tienda;

    @Id
    @Column(name = "id_empleado")
    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "edad")
    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Basic
    @Column(name = "fecha_inicio")
    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleado empleado = (Empleado) o;
        return idEmpleado == empleado.idEmpleado &&
                edad == empleado.edad &&
                Objects.equals(nombre, empleado.nombre) &&
                Objects.equals(fechaInicio, empleado.fechaInicio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEmpleado, nombre, edad, fechaInicio);
    }

    @OneToMany(mappedBy = "Empleado")
    public List<Pedido> getPedidos() {
        return Pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        Pedidos = pedidos;
    }

    @ManyToOne
    @JoinColumn(name = "id_tienda", referencedColumnName = "id_tienda", nullable = false)
    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }
}
