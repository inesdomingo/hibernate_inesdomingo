package com.inesd.hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "regalo_tienda", schema = "tienda_regalos", catalog = "")
public class Seccion {
    private int idRtienda;
    private String nombreSeccion;

    @Id
    @Column(name = "id_rtienda")
    public int getIdRtienda() {
        return idRtienda;
    }

    public void setIdRtienda(int idRtienda) {
        this.idRtienda = idRtienda;
    }

    @Basic
    @Column(name = "nombre_seccion")
    public String getNombreSeccion() {
        return nombreSeccion;
    }

    public void setNombreSeccion(String nombreSeccion) {
        this.nombreSeccion = nombreSeccion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seccion seccion = (Seccion) o;
        return idRtienda == seccion.idRtienda &&
                Objects.equals(nombreSeccion, seccion.nombreSeccion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRtienda, nombreSeccion);
    }
}
